all: prepare build run
build:
	yarn build;
	# sudo cp -a /home/app/something-client/out/. /usr/share/nginx/html;
	# rm -rf ./dist;
	# rm -rf ./out;
dev: prepare
	yarn dev;
prepare:
	yarn install --frozen-lockfile
run: 
	screen -S next yarn start