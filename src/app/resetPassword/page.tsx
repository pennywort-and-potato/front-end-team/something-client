'use client'
import React, { useEffect, useRef, useState } from 'react'
import HCaptcha from '@hcaptcha/react-hcaptcha';
import { resetPassword, sentCodeResetPassword } from '@/api/userApi';
import { Form } from '@/components/dataEntry/Form';
import { useRouter } from 'next/navigation';
import { message } from 'antd';

const axios = require('axios');

export default function ResetPassword() {
	const [token, setToken] = useState("");
	const captchaRef = useRef<any>();
	const [sendGmail, setSendGmail] = useState<any>("");
	const [loadingButton, setLoadingButton] = useState<boolean>(false);
	const [textError, setTextError] = useState<string>("");
	const router = useRouter()
	const [messageApi, contextHolder] = message.useMessage();
	const onLoad = () => {
		//captchaRef.current.execute();
	};

	useEffect(() => {

	}, [token]);

	const handleSubmitSendGmail = (e:any) => {
		e.preventDefault()
		const formData = new FormData(e.target)
		const stringFormData =  Object.fromEntries(formData)
		
		if(stringFormData["h-captcha-response"]) {
			let data = JSON.stringify(stringFormData);
			let config = {
				method: 'post',
				maxBodyLength: Infinity,
				url: 'https://www.pnp.name.vn/mailer/RequestResetPassword',
				headers: { 
					'Content-Type': 'application/json'  
				},
				data : data
			};
			setLoadingButton(true)
			axios.request(config)
			.then((response:any) => {
				setLoadingButton(false)
				if(response.data)  setSendGmail(stringFormData["email"])
				else console.log("eree")
			})
			.catch((error:any) => {
				setLoadingButton(false)
				captchaRef.current.execute();
				messageApi.open({
					type: 'error',
					content: 'Address not found',
				});
			});
		} else {

		}
	}

	const inputs = [
		{
			id: 1,
			name: "verificationCode",
			type: "text",
			placeholder: "Code",
			label: `We sent a code to gmail: ${sendGmail}`,
			errorMessage: "ádada",
			require:"true",
		},
		{
			id: 2,
			name: "newPassword",
			type: "password",
			placeholder: "NewPassword",
			label: "NewPassword",
			errorMessage: "ádada",
			require:"true",
		}
	]

	const handleSubmitResetPassword = (e:any) => {
		e.preventDefault()
		const formData = new FormData(e.target)
		const stringFormData =  Object.fromEntries(formData)
		const { newPassword, verificationCode } = stringFormData
		resetPassword(sendGmail, verificationCode, newPassword).then((response:any) => {
			if(response.id) router.push("/login")
			else messageApi.open({
				type: 'error',
				content: 'Wrong code! Try again',
			});
		})
	}

	return (
		<div className='login '>
			{contextHolder}
			{
				sendGmail == "" ?
				<div className='login_body form '>
					<div>
						<h2 className='flex flex_center'>ResetPassword</h2>
						<form onSubmit={handleSubmitSendGmail}>
							<div className='formInput padding-h-8'>
								<label className='formInput_label'> Gmail </label>
								<input className='formInput_input' required type="text" name="email" placeholder="Email" />
							</div>

							<div className='form_hcapcha'>
								<HCaptcha
									sitekey="4528f419-7c37-417c-ab4e-5989a4c9229f"
									onLoad={onLoad}
									onVerify={setToken}
									ref={captchaRef}
								/>
							</div>
							<button className='form_botton flex flex_center'>
								{
									loadingButton &&
									<div className='padding-w-8'>
										<div className='loading'></div>
									</div>
								}
								<div>
									Continue
								</div>
							</button>
						</form>
					</div>
				</div>
				:
				<div className='fullWidth'>
					<Form 
						submitName="ResetPassword"
						nameForm='ResetPassword'
						handleSubmit={handleSubmitResetPassword}
						inputs={inputs}
					/>
				</div>
			}
		 
		</div>
		
	);
}


