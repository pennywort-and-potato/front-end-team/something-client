import { Metadata } from "next";
import "./globals.scss";
import MainHOC from "@/hoc/MainHOC";
import Header from "@/components/layout/Header";
import Footer from "@/components/layout/Footer";
import Head from "next/head";
type Props = {
  children: React.ReactNode
}

export const metadata: Metadata = {
  title: "Something",
};

function RootLayout({ children }: Props) {
  return (
    <MainHOC>
      
      <html lang="en">  
        <Head>
          <meta property="og:image" content=""/>
        </Head> 
        <body>
          <Header/>
            {children}
          <Footer/>
        </body>
      </html>
     
    </MainHOC>
  );
}

export default RootLayout