'use client'
import React from 'react'
import {Form} from '@/components/dataEntry/Form'
import Link from 'next/link'
import { userLogin } from '@/api/userApi'

import { useLocalStorage } from 'usehooks-ts'
import { useRouter } from 'next/navigation'

const inputs = [
	{
		id: 1,
		name: "username",
		type: "text",
		placeholder: "Username",
		label: "Username",
		errorMessage: "lo áda",
		require:"true"
	},
	{
		id: 2,
		name: "password",
		type: "password",
		placeholder: "Password",
		label: "Password",
		errorMessage: "ádada",
		require:"true",
	},
	{
		id: 3,
		name: "keepLogin",
		type: "checkbox",
		placeholder: "Remember me",
		label: "Remember me",
		errorMessage: "ádada",
		require:"false"
	}
]

export default function Login() {
    
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const router = useRouter()


  const handleSubmit = async (e:any) => {
		e.preventDefault()
		const formData = new FormData(e.target)
	    await userLogin(formData).then(res =>  {
			if(res){
				setJwt(res.token)
				router.push("/")
			}
		})	
		
	}

    return (
        <div className='login '>
			<div className='login_body'>
				<Form 
					submitName="Login"
					nameForm='Login here'
					handleSubmit={handleSubmit}
					inputs={inputs}
				/>
				<div className='flex flex_center  form_footer'>
					<Link className='Login_next' href="/resetPassword" >Forgot password?</Link>
				</div>
				<div className='flex flex_center' >
					<span>Do not have an account ?</span>
					<Link className='Login_next' href="/register" >Sign up</Link>
				</div>
				
			</div>
            
            
        </div>
    )
}


