'use client'
import { getPostAll } from '@/api/post'
import ContentWrapper from '@/components/layout/ContentWrapper'
import TestComponent from '@/components/TestComponent'
import React, { useEffect, useState } from 'react'
import { useLocalStorage } from 'usehooks-ts'
import Image from 'next/image'
import InfiniteScroll from '@/components/event/InfiniteScroll'
import IndexWrapper from '@/components/layout/IndexWrapper'
import { getFollowPost, getFollowUser } from '@/api/follow'
import { useSelector } from 'react-redux'
import { RootState } from '@/store'
import { GetAllBookmark } from '@/api/bookmark'
import { HomeOutlined, TeamOutlined } from '@ant-design/icons'
import ListUser from '@/components/layout/ListUser'
import LoadBoxContent from '@/components/loading/LoadBoxContent'
import LoadBoxUser from '@/components/loading/LoadBoxUser'
import { MediaQuery } from '@/components/event/Media'

function Home() {
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	
	const user = useSelector((state: RootState) => state.user);
	const [dataHome, setDataHome] = useState<any>([{currentPage:0,totalPage:1000}]);
	const [page, setPage] = useState(1);
	const [tapFollow, settapFollow ] = useState<any>(false);
	const [followData, setFollowData] = useState<Array<any>>([]);
	let fistRenderFollow = true
	const [isLoadHome, setIsLoadHome] = useState<boolean>(true);
	const [isLoadFolow, setIsLoadFolow] = useState<boolean>(true);

	const [ followUserData, setFollowUserData ] = useState<any>();
	const [ allBookmark, setAllBookmark ] = useState()
	useEffect(() => {
		if(jwt != "") getFollowUser(jwt).then(data => {
			 setFollowUserData(data)
		})

		
	},[user])
	const loadMoreItems = async () => {
		let lastData = dataHome[dataHome.length - 1]
		if(lastData?.currentPage < lastData?.totalPage && !tapFollow)  await getPostAll(lastData.currentPage + 1).then(res => {
			setDataHome([...dataHome, {data: res.data, currentPage:res.currentPage, totalPage:res.totalPage}])
			setIsLoadHome(false)
			setTimeout(() => {
				
			}, 1000);
		})
		
		
	};
	const loadMoreItemsFolow = async () => {
		if (tapFollow && isLoadFolow) {
			if(jwt) await getFollowPost(jwt).then(data => {
				let dataFix = data.data.filter((item:any) => item)
				if(dataFix) {
					if (dataFix.length == 0) setIsLoadFolow(false)
					else setIsLoadFolow(true)
					setFollowData(dataFix)
				}
			})
		}
	}
	useEffect(() => {
		if(jwt != "") GetAllBookmark(jwt).then(async data => {
				await setAllBookmark(data)
		})

	},[jwt])


	return (
		<main id='root'>
		
			{/* {
				user.id != 0 &&
				<div className='Index_container'>
					<div className='index_menu flex'>
						<div className={!tapFollow ? 'index_menu_item active' : "index_menu_item" } onClick={() => settapFollow(false)}>Dành cho bạn</div>
						<div className={tapFollow ? 'index_menu_item active' : "index_menu_item" } onClick={() => settapFollow(true)}>Đang follow</div>
					</div>
				</div>
			} */}

			{	!MediaQuery(1200) &&
				<div className='index_left'>
					<div className={!tapFollow ? 'index_left_item active' : "index_left_item" } onClick={() => settapFollow(false)}>
						<div><HomeOutlined /></div>
						<div className='index_left_item_tiltel'>Dành cho bạn</div>
					</div>
					<div className={tapFollow ? 'index_left_item active' : "index_left_item"} onClick={() => settapFollow(true)}>
						<div><TeamOutlined /></div>
						<div className='index_left_item_tiltel'>Đang folow</div>
					</div>
					<div className='endtag'></div>
					
					{	
						user.id  != 0 &&
						(
							followUserData ?
								<div className='index_left_listUser'>
									<ListUser data={followUserData}/>
								</div>
							:
							<div className='index_left_listUser'>
								<LoadBoxUser/>
							</div>
						)
					}
					
				</div>

		
			}
			
			{
				!tapFollow ?
				(
					dataHome.length > 0 && dataHome[dataHome.length - 1]?.data ? (
						<>
							{	
								dataHome.map((dataPost: any,id:number) => {
									if(dataPost.data) return <IndexWrapper allBookmark={allBookmark} followUserData={followUserData} key={id} data={dataPost.data.map((itemPost: any) => {
										const newItemPost = {
											...itemPost.post,
											user: itemPost.user,
											image: itemPost.medias.length != 0 ? itemPost.medias[0].fileName : "",
											likeList: itemPost.likeList,
											listImage: itemPost.medias
										}
										return newItemPost
									})}/>
								})
							}
							
							
						</>
						
					):
					<LoadBoxContent/>
				)
				:
				(
					followData ? 
					(
						<>
							{	
								followData.length != 0 ?
								<IndexWrapper followUserData={followUserData} data={followData.map((itemPost: any) => {
									const newItemPost = {
										...itemPost.post,
										user: itemPost.user,
										image: itemPost.medias.length != 0 ? itemPost.medias[0].fileName : "",
										likeList: itemPost.likeList,
										listImage: itemPost.medias
									}
									return newItemPost
								})}/>
								: <div className='Index_container padding-h-50'>
											<div className='fontSize-20 flex flex_center'>You haven&#39;t followed anyone</div>
								</div>
							}
							
						</>
					)
					:
					<LoadBoxContent/>
				)
			}
			
			
			<InfiniteScroll onLoadMore={loadMoreItems} isload={isLoadHome}/>
			<InfiniteScroll onLoadMore={loadMoreItemsFolow}  isload={isLoadFolow}/>
		</main>
	)
}


export default Home