'use client'

import { getFollowUser } from '@/api/follow'
import { seacrchPost, seacrchUser } from '@/api/search'
import IndexWrapper from '@/components/layout/IndexWrapper'
import ListUser from '@/components/layout/ListUser'
import LoadBoxContent from '@/components/loading/LoadBoxContent'
import { RootState } from '@/store'
import { useRouter } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useLocalStorage } from 'usehooks-ts'


function Search(props:any) {
	const router = useRouter()
	let NameQuary = props.searchParams.q
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const user = useSelector((state: RootState) => state.user);
	const [dataSearchPost, setDataSearchPost] = useState<any>();
	const [dataSearchUser, setDataSearchUser] = useState<any>();

	const [ followUserData, setFollowUserData ] = useState<any>();

	useEffect(() => {
		
		if(jwt != "") getFollowUser(jwt).then(dataUser => {
			 setFollowUserData(dataUser)
		})
		
		seacrchPost(NameQuary, jwt).then((data:any) => {
			if(data?.data) {
				setDataSearchPost(data.data.data)
			}else  setDataSearchPost([])
		})
		seacrchUser(NameQuary, jwt).then((data:any) => {
			if(data?.data) {
				setDataSearchUser(data.data.data)
			} else  setDataSearchPost([])
		})
	},[jwt])
	
	return (
		<>
			{
				dataSearchUser?.length>0 &&
				<div className='margin-t-20 ' >
					<div className='Index_container background margin-w-14 border-r-8 padding-h-2'>
						<div className='padding-w-14 '>
							<ListUser data={dataSearchUser} propsFollow={true} size={'big'}/>
						</div>
					</div> 
				</div>
				
			}
		 
			{	
				dataSearchPost ?
					dataSearchPost.length > 0 ?
					<IndexWrapper followUserData={followUserData} data={dataSearchPost?.map((itemPost: any) => {
						const newItemPost = {
							...itemPost.post,
							user: itemPost.user,
							image: itemPost.medias.length != 0 ? itemPost.medias[0].fileName : "",
							likeList: itemPost.likeList,
							listImage: itemPost.medias
						}
						return newItemPost
					})}/>
					: dataSearchUser	? "" 
						:<div className='Index_container padding-h-50'>
									<div className='fontSize-20 flex flex_center'>No posts found matching this keyword.</div>
						</div>
				: <LoadBoxContent/>
			}
		</>
	)
}


export default Search