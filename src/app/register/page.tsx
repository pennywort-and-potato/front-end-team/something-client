'use client'
import React, { useEffect } from 'react'
import { Form }from '@/components/dataEntry/Form'
import Link from 'next/link'
import { userRegister } from '@/api/userApi'
import { uploadSingle } from '@/api/uploader'
import { useLocalStorage } from 'usehooks-ts'
import { useRouter } from 'next/navigation'
const inputs = [
	{
		id: 1,
		name: "username",
		type: "text",
		placeholder: "Username",
		label: "Username",
		errorMessage: "lo áda",
		require:"true"
	},
	{
		id: 2,
		name: "email",
		type: "text",
		placeholder: "Email",
		label: "Email",
		errorMessage: "ádada",
		require:"true"
	},
	{
		id: 3,
		name: "password",
		type: "password",
		placeholder: "Password",
		label: "Password",
		errorMessage: "ádada"
	},
	{
		id: 4,
		name: "firstName",
		type: "text",
		placeholder: "FirstName",
		label: "FirstName",
		errorMessage: "ádada"
	},
	{
		id: 5,
		name: "lastName",
		type: "text",
		placeholder: "LastName",
		label: "LastName",
		errorMessage: "ádada"
	},
	{
		id: 6,
		name: "dateOfBirth",
		type: "date",
		placeholder: "DateOfBirth",
		label: "DateOfBirth",
		errorMessage: "ádada"
	}
]

export default function Login() {
	const router = useRouter()

    const handleSubmit = async (e:any) => {
		e.preventDefault()
		const formData = new FormData(e.target)
        const data = Object.fromEntries(formData.entries())

		userRegister(formData).then(data => {
			if(data)	router.push("/login")	
			
		})
	}

	

    return (
        <div className="login Register">
			<div className='login_body'>
				<Form 
					submitName="Register"
					nameForm='Register'
					handleSubmit={handleSubmit}
					inputs={inputs}
				/>
				<div className='flex flex_center form_footer' >
					<span>Have an account?</span>
					<Link className='Login_next' href="/login" >Sign in</Link>
				</div>
			</div>
            
        </div>
    )
}
