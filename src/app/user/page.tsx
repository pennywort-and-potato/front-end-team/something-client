'use client'
import Image from 'next/image'
import React, { useEffect, useState } from 'react'
import ContentWrapper from '@/components/layout/ContentWrapper'
import PostSvg from '@/components/svg/upLoad.svg'
import SaveSvg from '@/components/svg/save.svg'
import MoreVertica  from '@/components/svg/moreVertica.svg'
import { getPostAll, getPostMe } from '@/api/post' 
import { useLocalStorage } from 'usehooks-ts'
import { useSelector } from 'react-redux'
import { RootState } from '@/store'
import NavItemFollow from '@/components/display/navItemFollow'
import { GetAllBookmark } from '@/api/bookmark'


export default function User(props:any) {
	
	const [data, setData] = useState<any>([]);
	const [toggleState, settoggleState] = useState<any>(1)

	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const user  = useSelector((state: RootState) => state.user)
	const [ allBookmark, setAllBookmark ] = useState<any>()
	useEffect(() => {
		if(jwt) {
			getPostMe(jwt).then(data => {
				if(data ) setData(data)
			})
			GetAllBookmark(jwt).then(async data => {
				setAllBookmark(data)
			})
		}
		
	}, [jwt,user]);


	return(
		(
			user.firstName ?
			(
				<div className="User ">
					<div className='User_header flex flex_center'>
						<div className="User_header_user radius-4">
							
							<div className='User_header_user_avatar flex flex_center'>
								{user?.avatar?.includes("jpg") || user?.avatar?.includes("png")
									?
									(
										< Image className='radius-4' alt="user" width={75} height={75} src={"https://f005.backblazeb2.com/file/PnP-Bucket/" + data.avatar}/>	
									)
									:
									(
										<div className='User_header_user_avatar_text flex flex_center'>
											{user.lastName[0]}
										</div>
									)
							
								}
								

							</div>
							<div className='User_header_conten flex flex_center'>
								{user.firstName} {user.lastName}
							</div>
							<div className='User_header_email flex flex_center'>
								{user.email}
							</div>
							<NavItemFollow data={data} />
							<div className='User_header_edit flex flex_center'>
								<div className='User_header_edit_item'>Share</div>
								<div className='User_header_edit_item'>Edit profile</div>
							</div>

						</div>
						<div className='User_header_menu flex flex_center'>
							<div
								onClick={() => settoggleState(1)}
							 	className={(toggleState == 1 ? "active " : " ") + "User_header_menu_item flex flex_center"}>
								
								<div className='User_header_menu_item_name'>Post</div>
							</div>
							<div
								onClick={() => settoggleState(2)}
							 	className={(toggleState == 2 ? "active " : " ") + "User_header_menu_item flex flex_center"}>
								
								<div className='User_header_menu_item_name'>Media</div> 
							</div>
							<div
								onClick={() => settoggleState(3)}
								className={(toggleState == 3 ? "active " : " ") + "User_header_menu_item flex flex_center"}>
								
								<div className='User_header_menu_item_name'>Saved</div> 
							</div>
							<div
								onClick={() => settoggleState(4)}
								className={(toggleState == 4 ? "active " : " ") + "User_header_menu_item flex flex_center"}>
								
								<div className='User_header_menu_item_name'>Hidden</div> 
							</div>

						</div>
					</div>
					
					<div className="User_body">
						{toggleState == 1 && <ContentWrapper allBookmark={allBookmark} deletePost={true} data={data.map((itemPost: any) => {
							const newItemPost = {
								...itemPost.post,
								image: itemPost.medias.length != 0 ? itemPost.medias[0].fileName : "",
								likeList: itemPost.likeList
							}
							return newItemPost
						})}/> }
						{toggleState == 2 && <ContentWrapper allBookmark ={allBookmark} deletePost={false} data={data.reduce((arr: any, item: any) => {
							item.medias.map((media: any) => {
								media.image = media.fileName
								return media
							})
							return [...arr,...item.medias]
						},[])}/> }
						{toggleState ==3 && allBookmark &&  <ContentWrapper allBookmark={allBookmark} deletePost={true} data={allBookmark.posts.map((itemPost: any) => {
							const newItemPost = {
								...itemPost.post,
								image: itemPost.medias.length != 0 ? itemPost.medias[0].fileName : "",
								likeList: itemPost.likeList
							}
							return newItemPost
						})}/> }
					

						{toggleState == 4 && <ContentWrapper deletePost={true} data={data.reduce((arr: any, item: any) => {
							const newItemPost = {
								...item.post,
								image: item.medias.length != 0 ? item.medias[0].fileName : ""
							}
							return newItemPost.isPrivate ? [...arr,newItemPost] : arr
						},[])}/> }

					</div>
				</div>

			):
			(
				<div className='flex flex_center'>
					
					{/* <Image className='radius-4' alt="user" width={308} height={231} src="/image/loadingdots2.gif"/> */}
				</div>
			)

		)
		
	)
}