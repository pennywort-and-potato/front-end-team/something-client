'use client'

import React, { ReactNode, useEffect, useState } from "react";
import { store } from "../store";
import { Provider, useDispatch } from 'react-redux'
import { useLocalStorage } from "usehooks-ts";
import { disposeUser, setUser } from "@/features/userSlice";
import { userGet } from "@/api/userApi";

type Props = { children: ReactNode };

const Middleware = ({ callback }: any) => {

  const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
  const dispatcher = useDispatch()

  useEffect(() => {
    if (jwt) {
      userGet(jwt)
      .then((res: any) => dispatcher(setUser(res)))
    }
    else {
      dispatcher(disposeUser())
    }
  }, [jwt])

  return <></>
}

const MainHOC = ({ children }: Props) => {
  return (
    <Provider store={store}>
      <Middleware />
      {children}
    </Provider>)
};

export default MainHOC;
