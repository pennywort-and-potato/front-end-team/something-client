import axios from "axios";
import { apiUrl } from "@/utils/common";

const instance = axios.create({
    //  withCredentials: true,
    baseURL: apiUrl
  })
  
export async function GetPostComment(id:number, page:number,token:any) { 
      try {
        instance.defaults.headers.common['Authorization'] = token;
        const res = await instance.get(`Props/GetPostComments?postId=${id}&page=${page}`);
        return res.data;
      } catch (err) {
        return console.log(err);
      }
  }

export async function Comment(idPost:number, body:any,token:any) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.post(`Comment?postId=${idPost}&body=${body}`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}

export async function DeleteComment(idComment:number, token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.delete(`Comment?commentId=${idComment}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function LikeComment(idComment:number,token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.post(`Comment/Like?commentId=${idComment}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function UnLikeComment(idComment:any,token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.post(`Comment/UnLike?commentId=${idComment}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}