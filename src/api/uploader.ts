import axios from "axios";
import { uploadUrl } from "@/utils/common";

// Using FormData for test purpose, will change later

const instance = axios.create({
  //  withCredentials: true,
  baseURL: uploadUrl
})




export async function uploadSingle(params: FormData, token:any) { 


  try {
    const res = await instance.put(`uploadImage/single`,params ,{
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}