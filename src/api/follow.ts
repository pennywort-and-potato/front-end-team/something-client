import axios from "axios";
import { apiUrl } from "@/utils/common";

// Using FormData for test purpose, will change later

const instance = axios.create({
  //  withCredentials: true,
  baseURL: apiUrl
})

export async function follow(token:any,id:number) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.post(`Follow?userId=${id}`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}

export async function unFollow(token:any,id:number) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.delete(`Follow?userId=${id}`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}
export async function getFollowUser(token:any) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.get(`Follow/Following`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}
export async function getFollower(token:any) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.get(`Follow/Follower`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}
export async function getFollowPost(token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.get(`Follow/FollowingUserpost`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}
  