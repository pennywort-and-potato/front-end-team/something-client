import axios from "axios";
import { apiUrl } from "@/utils/common";

// Using FormData for test purpose, will change later

const instance = axios.create({
  //  withCredentials: true,
  baseURL: apiUrl
})
const instanceRequest = axios.create({
  //  withCredentials: true,
  baseURL: "https://www.pnp.name.vn/mailer/"
})


export async function userGet(token: string) {
  try {
    const res = await instance.get('Me', {
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function userIdGet(id:number) {
  try {
    const res = await instance.get(`/Props/GetUserById?userId=${id}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function userIdToPost(id:number,token: string) {
  try {
    const res = await instance.get(`Post/User/${id}`, {
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function userLogin(params: FormData) {
  try {
    const res = await instance.post('Login', params);
    return res.data;
  } catch (err) {
    if(err) alert("Login false")
    return console.log(err);
  }
}

export async function userRegister(params: FormData) {
  try {
    const res = await instance.post('Register', params);
    return res.data;
  } catch (err:any) {
    if(err?.response?.data) alert(err.response.data)  
    return console.log(err);
  }
}

export async function userLogout(params: FormData) {
    try {
      const res = await instance.post('Logout', params);
      return res.data;
    } catch (err:any) {
      return console.log(err);
    }
}

export async function sentCodeResetPassword(params: any) {
  try {
    instanceRequest.defaults.headers.common['Content-Type'] = 'application/json';
    instanceRequest.defaults.headers.common['data'] = params;
    const res = await instanceRequest.post(`RequestResetPassword`);
    
    return res.data;
  } catch (err:any) {
    return console.log(err);
  }
}

export async function resetPassword(email: any, code: any,passNew: any) {
  try {
    const res = await instance.post(`Props/ResetPassword?email=${email}&verificationCode=${code}&newPassword=${passNew}`);
    return res.data;
  } catch (err:any) {
    return err;
  }
}

