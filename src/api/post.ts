import axios from "axios";
import { apiUrl } from "@/utils/common";

// Using FormData for test purpose, will change later

const instance = axios.create({
  //  withCredentials: true,
  baseURL: apiUrl
})




export async function getPostMe(token:any) { 
  try {
    const res = await instance.get(`Post/CurrentUser`,{
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function getPostAll(page: number) { 
  try {
    const res = await instance.get(`Post/Recent?page=${ page }`,{
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function createPost(params:any,token:any) { 
  try {
    const res = await instance.post(`Post`,params,{
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    alert("Please enter post information")
    return console.log(err);
  }
}

export async function addContentoPost(params:any,token:any) { 
  try {
    const res = await instance.put(`Post`,params,{
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function getPost(id:number, token:any) { 
  try {
    const res = await instance.get(`/Props/GetPostById?postId=${id}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function deletePost(id:number, token:any) { 
  try {
    const res = await instance.delete(`/Post?postId=${id}`,{
      headers: {
        'Authorization': token
      } 
    });
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function LikePost(id:number, token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.put(`/Post/Like?postId=${id}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}
export async function UnLikePost(id:number, token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.put(`/Post/UnLike?postId=${id}`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}



