import axios from "axios";
import { apiUrl } from "@/utils/common";

// Using FormData for test purpose, will change later

const instance = axios.create({
  //  withCredentials: true,
  baseURL: "https://www.pnp.name.vn/api/v2/"
})


export async function seacrchPost(searchText:string, token:string) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.get(`Search/Post?query=${searchText}&page=1`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function seacrchUser(searchText:string, token:string) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.get(`Search/User?query=${searchText}&page=1`);
    return res.data;
  } catch (err) {
    return err;
  }
}
