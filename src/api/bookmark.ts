import axios from "axios";
import { apiUrl } from "@/utils/common";

const instance = axios.create({
    //  withCredentials: true,
    baseURL: apiUrl
})
export async function AddBookmark(id:number, token:any) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.post(`Bookmark?postId=${id}`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}

export async function GetAllBookmark(token:any) { 
  try {
    instance.defaults.headers.common['Authorization'] = token;
    const res = await instance.get(`Bookmark`);
    return res.data;
  } catch (err) {
    return console.log(err);
  }
}

export async function UnBookmark(id:number, token:any) { 
    try {
      instance.defaults.headers.common['Authorization'] = token;
      const res = await instance.delete(`Bookmark?postId=${id}`);
      return res.data;
    } catch (err) {
      return console.log(err);
    }
}