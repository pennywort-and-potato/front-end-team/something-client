export interface IUser {
  id: number
  username: string
  email: string
  firstName: string
  lastName: string
  dateOfBirth: string
  avatar: string
}

export interface IUserRegister {
  username: string
  email: string
  password: string
  firstName?: string
  lastName?: string
  dateOfBirth?: string
  avatar?: string
}

export interface IUserLogin {
  username: string
  password: string
  keepLogin: boolean
}

export interface IPost {
  body: string,
  iat: string,
  id: number,
  isPrivate : boolean,
  like: number,
  nsfw: boolean,
  title: string,
  view: number
}

export interface Imedia {
  body: string,
  fileId: string,
  fileName?: string,
  id: number,
  like: number,
  nsfw: boolean,
  title: number,
  view: number
}

export interface ILike {
  id: number,
  username: string,
  email: string,
  firstName?: string,
  lastName?: string,
  dateOfBirth: string,
  avatar: string | null
}

export interface IPosts {
  likeList?: Array<ILike>,
  medias?: Array<Imedia>,
  post: IPost,
  user: IUser
}

export interface IDataPost {
  currentPage: number,
  data: Array<object| null> | null,
  totalPage: number
}

export interface IDataUser {
  currentPage: number,
  data: Array<IUser| null> | null,
  totalPage: number
}


