import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { IUser } from '../interface/userInterface'

const initialState: IUser = {
  id: 0,
  username: '',
  email: '',
  firstName: '',
  lastName: '',
  dateOfBirth: '',
  avatar: ''
}

export const userState = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<IUser>) {
      state.id = action.payload.id
      state.username = action.payload.username
      state.email = action.payload.email
      state.firstName = action.payload.firstName
      state.lastName = action.payload.lastName
      state.dateOfBirth = action.payload.dateOfBirth
      state.avatar = action.payload.avatar
    },
    disposeUser(state) {
      state.id = 0
      state.username = ''
      state.email = ''
      state.firstName = ''
      state.lastName = ''
      state.dateOfBirth = ''
      state.avatar = ''
    }
  },
})

export const { setUser, disposeUser } = userState.actions
export default userState.reducer