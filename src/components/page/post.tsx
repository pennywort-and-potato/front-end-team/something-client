"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import { addContentoPost, deletePost, getPost } from "@/api/post";
import { useLocalStorage } from "usehooks-ts";
import Link from "next/link";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import {
	DashOutlined,
	DeleteOutlined,
	DownloadOutlined,
	EllipsisOutlined,
	LinkOutlined,
	MoreOutlined,
	PlusOutlined,
	SettingOutlined,
	ShareAltOutlined,
	SyncOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/navigation";
import Dropdown from "../Navigation/Dropdown";
import { Button, Col, Form, Modal, Row, Upload, UploadFile } from "antd";
import axios from "axios";
import InforAvatar from "../display/InforAvatar";
import { getFollowUser } from "@/api/follow";
import NavItemPost from "../display/navItemPost";
import CommentPost from "../display/commentPost";
import { GetAllBookmark } from "@/api/bookmark";

function PostRender(props: any) {
	const [data, setData] = useState<any>();
	const [jwt, setJwt] = useLocalStorage("user_jwt", "");
	const user = useSelector((state: RootState) => state.user);
	const router = useRouter();

	const [isModalOpen, setIsModalOpen] = useState(false);
	const [fileList, setFileList] = useState<UploadFile[]>([]);
	const [ followUserData, setFollowUserData ] = useState<any>();
	const [ allBookmark, setAllBookmark ] = useState()
	const handleCancel = () => setIsModalOpen(false);
	const handAddContentPost = () => setIsModalOpen(true);

	async function getPostApi() {
		if(jwt) await GetAllBookmark(jwt).then(dataBookmark => {

			setAllBookmark(dataBookmark)
		})

		if (props.idPost) 
			await getPost(props.idPost, jwt).then((res) => {
				if (res) setData(res);
				
			});
	}
	useEffect(() => {
		getPostApi()
	}, [jwt]);
	
	const handleDeletePost = () => {
		deletePost(data.post.id, jwt).then((res: any) => {
			if (res) {
				router.push("/");
			}
		});
	};
	const handleRemove = (file: any) => {
		const updatedFileList = fileList.filter((item) => item.uid !== file.uid);
		setFileList(updatedFileList);
	};
	const onPreview = () => {};

	// uploadImage
	const handleUpload = async ({
		file,
		onSuccess,
		onError,
		fileList: newFileList,
	}: any) => {
		const formData = new FormData();
		setFileList([
			...fileList,
			{
				uid: file.uid,
				name: "response.data.fileName",
				status: "uploading",
			},
		]);
		formData.append("image", file);
		try {
			const response = await axios.put(
				"https://www.pnp.name.vn/uploader/uploadImage/single",
				formData,
				{
					headers: {
						Authorization: jwt,
					},
				}
			);
			if (response) {
				setFileList([
					...fileList,
					{
						uid: response.data.data.fileId,
						name: "response.data.fileName",
						status: "done",
						url:
							"https://f005.backblazeb2.com/file/PnP-Bucket/" +
							response.data.data.fileName,
					},
				]);
			}
		} catch (error) {
			setFileList([
				...fileList,
				{
					uid: file.uid,
					name: "image.png",
					status: "error",
				},
			]);
		}
	};
	// add post
	const onFinish = async (values: any) => {
		if (props.idPost) {
			const formDataAddConten = new FormData();
			formDataAddConten.append("postId", `${props.idPost}`);
			let checkImageDone = false;
			fileList.map((item: any) => {
				if (item.status == "done") {
					formDataAddConten.append("mediaId", item.uid);
					checkImageDone = true;
				}
			});
			if (checkImageDone) {
				await addContentoPost(formDataAddConten, jwt).then((res) => {
					if (res) {
						setData(res);
						setIsModalOpen(false);
						setFileList([]);
					}
				});
			} else {
			}
		} else {
		}
	};

	const uploadButton = (
		<div>
			<PlusOutlined />
			<div style={{ marginTop: 8 }}>Upload</div>
		</div>
	);
	
	const copyLink = (fileName:any) => {
		const linkImage = fileName ? "https://f005.backblazeb2.com/file/PnP-Bucket/" +fileName : "null"
		navigator.clipboard
			.writeText(linkImage)
			.then(() => {})
			.catch((error) => {});
		return true
	};

	

	const downloadLink = (fileName:any) => {
		const linkImage = fileName ? "https://f005.backblazeb2.com/file/PnP-Bucket/" +fileName : "null"
		const link = document.createElement('a');
		link.href = linkImage;
		link.download = fileName;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
	const itemsUser = [
		{
			key: "1",
			label: (
				<div onClick={handAddContentPost}>
					<SyncOutlined style={{ marginRight: "8px" }} />
					Update
				</div>
			),
		},
		{
			key: "2",
			label: (
				<div>
					<ShareAltOutlined style={{ marginRight: "8px" }} />
					Share
				</div>
			),
		},
		{
			key: "3",
			label: (
				<div onClick={handleDeletePost}>
					<DeleteOutlined style={{ marginRight: "8px" }} />
					Delete
				</div>
			),
		},
	];

	useEffect(() => {
		if(jwt != "")getFollowUser(jwt).then(data => {
			setFollowUserData(data)
		})
	},[jwt])
	const editImage = (fileName:any) =>{
		return [
			{
				key: "1",
				label: (
					<div onClick={() => copyLink(fileName)}>
						<LinkOutlined style={{ marginRight: "8px" }} />
						Copy Link
					</div>
				),
			},
			{
				key: "2",
				label: (
					<div onClick={() => downloadLink(fileName)}>
						<DownloadOutlined style={{ marginRight: "8px" }} />
						Download
					</div>
				),
			},
		];
	} 
	const dataNav = data ? {
		...data.post,
		user: data.user,
		likeList: data.likeList,
		listImage: data.listImage
	} : {}
	


	return (
		<div className="Gallery">
			{data?.post ? (
				<div className="Gallery_body">
					

					<Row gutter={16}>
						<Col className="gutter-row" md={{span: 12, order: 1}}>
							{data?.medias?.length > 0 ? (
								<div className="Gallery_listImg">
									{data.medias.map((item: any, id: number) => {
										return (
											<div key={id} className="Gallery_listImg_container">
												<Image
													className="Gallery_listImg_item"
													alt="description of image"
													style={{ width: "100%" }}
													height={300}
													width={300}
													src={
														item.fileName
															? "https://f005.backblazeb2.com/file/PnP-Bucket/" +
																item.fileName
															: "/image/noimage.jpg"
													}
												/>
												<div className="Gallery_listImg_more">
													<Dropdown className="dropdown" menu={editImage(item?.fileName)}>
														<EllipsisOutlined style={{ color: "#fff" }} />
													</Dropdown>
												</div>
											</div>
										);
									})}
								</div>
							) : (
								<Image
									alt="description of image"
									height={300}
									width={300}
									src={"/image/noimage.jpg"}
								/>
							)}
						</Col>
						<Col className="gutter-row" md={{span: 12, order: 2}}>
							<div className="Gallery_body_right padding-w-8">
								<div>
									
								</div>
								
								
								<div className="flex flex_justify">
									<div>
										<InforAvatar dataUser={data.user} dataUserLogin={user} date={data.post.iat} listUserFolow={followUserData}/>
									</div>
									<div>
									{user?.id == data.user.id && (
											<div className="Gallery_setting">
												<Dropdown className="dropdown" menu={itemsUser}>
													<MoreOutlined style={{ fontSize: "24px" }} />
												</Dropdown>
												<Modal
													open={isModalOpen}
													onCancel={handleCancel}
													footer={null}
													width={400}
												>
													<h2 className="flex flex_center">Add Image To Post</h2>
													<Form
														name="basic"
														labelCol={{ span: 24 }}
														wrapperCol={{ span: 24 }}
														style={{ maxWidth: 600 }}
														initialValues={{ remember: true }}
														onFinish={onFinish}
														autoComplete="off"
														className="formUplpad"
													>
														<Form.Item label="">
															<Upload
																listType="picture-card"
																fileList={fileList}
																onRemove={handleRemove}
																customRequest={handleUpload}
																onPreview={onPreview}
															>
																{fileList.length >= 8 ? null : uploadButton}
															</Upload>
														</Form.Item>
														<Form.Item wrapperCol={{ offset: 0, span: 0 }}>
															<Button
																className="buttonSubmit"
																type="primary"
																htmlType="submit"
															>
																Update
															</Button>
														</Form.Item>
													</Form>
												</Modal>
											</div>
										)}
									</div>
									
								</div>
								<h1 className="flex flex_center" style={{margin:"0px"}}>{data.post.title}</h1>
								<span className="Gallery_body_conten"> {data.post.body}</span>
								<div className="Gallery_body_navItem">
									<NavItemPost data={dataNav} allBookmark={allBookmark}/>
								</div>
								<CommentPost idPost={props.idPost}/>
								
							</div>
							
						</Col>
					</Row>
				</div>
			) : (
				<div></div>
			)}
		</div>
	);
}

export default PostRender;
