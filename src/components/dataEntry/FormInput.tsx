import React from 'react'
import styles from '../dataEntry/formInput.module.scss'
 const FormInput = (props :any)  =>{

	const { label, errorMessage, onChange, id,...inputProps } = props
	return (
	   <div className={styles.formInput}>
			<label className={styles.formInput_label}>{label}</label>
			<input className={styles.formInput_input} {...inputProps} onChange={onChange}  />
			<span className={styles.formInput_error}>{errorMessage}</span>
	   </div>
	)
}

export default FormInput