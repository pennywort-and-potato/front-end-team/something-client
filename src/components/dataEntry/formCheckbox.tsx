import React from 'react'
import styles from '../dataEntry/formInput.module.scss'
 const FormInputCheckbox = (props :any)  =>{

	const { label, errorMessage, onChange, id,...inputProps } = props
	return (
	   <div className={styles.formInputCheckbox}>
            <input className={styles.formInput_inputCheckbox} {...inputProps} onChange={onChange}  />
			<label className={styles.formInput_label}>{label}</label>
			<span className={styles.formInput_error}>{errorMessage}</span>
	   </div>
	)
}

export default FormInputCheckbox