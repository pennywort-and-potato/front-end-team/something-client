'use client'
import styles from '../dataEntry/formInput.module.scss'
import React, { useEffect, useRef, useState } from 'react'
import FormInput from './FormInput'
import FormInputCheckbox from './formCheckbox'
import SearchIcon from '@/components/svg/search.svg'
import { seacrchPost, seacrchUser } from '@/api/search'
import { useLocalStorage } from 'usehooks-ts'
import { IDataPost, IDataUser } from '@/interface/userInterface'
import Link from 'next/link'
import Image from 'next/image'
import TimeConvent from '../Feedback/TimeConvent'

 const Form = (props :any)  =>{
	const { inputs, handleSubmit, nameForm, submitName } = props

	const setValuesdef = inputs.reduce((obj: any,val: any ) => {
		return val.type == "checkbox" ? {...obj, [val.name] : false} : {...obj, [val.name] : ""}
	},{})
	const [values, setValues] = useState<any>(setValuesdef)
	
	const onChange = (e :any) => {
		let valInput = e.target.type == "checkbox" ? e.target.checked : e.target.value
		setValues({...values,[e.target.name]: valInput})
	}

	
	return (
		
		<form className={styles.form +" "+ styles.formInput_form_rgb} onSubmit={handleSubmit}>
			<h3>{nameForm}</h3>
			{
				inputs.map((input:any) => (
						input.type != "checkbox"
						? (
							<FormInput 
								key={input.id} 
								{...input} 
								value={values[input.name]}
								onChange={onChange}
						/>
						): (
							<FormInputCheckbox
								key={input.id} 
								{...input} 
								value={values[input.name]}
								onChange={onChange}
							/>
						)
				))
			}
			
			<button className={styles.formBotton}>{submitName && submitName}</button>
		</form>
	)
}


const FormUpload = (props :any)  => {
	const { inputs, handleSubmit, nameForm, submitName } = props

	const setValuesdef = inputs.reduce((obj: any,val: any ) => {
		return val.type == "checkbox" ? {...obj, [val.name] : false} : {...obj, [val.name] : ""}
	},{})
	const [values, setValues] = useState<any>(setValuesdef)

	const onChange = (e :any) => {
		let valInput = e.target.type == "checkbox" ? e.target.checked : e.target.value

		if (e.target.type == "file") {
			let arrFile = Array.from(e.target.files)
			console.log(arrFile)
		}
		
		setValues({...values,[e.target.name]: valInput})
	}

	return (

		<form className={styles.form +" "+ styles.formInput_form_rgb} onSubmit={handleSubmit}>
			<h3 className={styles.titel}>{nameForm}</h3>
			{
				inputs.map((input:any) => (
					input.type != "checkbox"
					?
					<FormInput 
						key={input.id} 
						{...input} 
						value={values[input.name]}
						onChange={onChange}
					/>
					:
					<FormInputCheckbox
						key={input.id} 
						{...input} 
						value={values[input.name]}
						onChange={onChange}
					/>
				))
			}
			
			<button className={styles.formBotton}>{submitName && submitName}</button>
		</form>
	)
	
}

const FormSeach = (props :any)  => {
	const { inputs, handleSubmit, nameForm, buttonSeach } = props
	const setValuesdef = inputs.reduce((obj: any,val: any ) => {
		return val.type == "checkbox" ? {...obj, [val.name] : false} : {...obj, [val.name] : ""}
	},{})
	const [values, setValues] = useState<any>(setValuesdef)
	
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')

	const [searchTerm, setSearchTerm] = useState<string>('');
	const [debouncedSearchTerm, setDebouncedSearchTerm] = useState<string>('');

	const [dataSearchPost, setDataSearchPost] = useState<any>();
	const [dataSearchUser, setDataSearchUser] = useState<any>();

	const [dropdownSearch , setDropdownSearch] = useState<boolean>(false);

	useEffect(() => {
		const delaySearch = setTimeout(() => {
			setDebouncedSearchTerm(searchTerm);
		}, 500);

		return () => {
			clearTimeout(delaySearch);
		};
	}, [searchTerm]);

	useEffect(() => {
		if (debouncedSearchTerm) {
			const searchData = async () => {
				await seacrchPost(debouncedSearchTerm, jwt).then((data:any) => {
					if(data?.data) {
						const arrPost = data.data.data.filter((post: any, id: number) => id < 4)
						setDataSearchPost(arrPost)
					} else setDataSearchPost(undefined)
				})
				await seacrchUser(debouncedSearchTerm, jwt).then((data:any) => {
					if(data?.data) {
						const arrUser = data.data.data.filter((user: any, id: number) => id < 4)
						setDataSearchUser(arrUser)
					} else setDataSearchUser(undefined)
				})

			}
			searchData()
		}else {
			setDataSearchPost(undefined)
			setDataSearchUser(undefined)
		}
	}, [debouncedSearchTerm]);

	const onChange = (e :any) => {
		let valInput = e.target.type == "checkbox" ? e.target.checked : e.target.value
		setValues({...values,[e.target.name]: valInput})

		setSearchTerm(valInput);
	}
	return (

		<form className={styles.form +" " +styles.formSeach} onSubmit={handleSubmit}>
			{	
				inputs.map((input:any) => (
					<FormInput 
						key={input.id} 
						{...input} 
						value={values[input.name]}
						onChange={onChange}
					/>
				))
			}
			{
				buttonSeach ?
					<div>
						<button className={styles.formBotton}><SearchIcon/></button>
					</div>
				:
				<button className={styles.formBotton}>Submit</button>
			}
			
			{
				(dataSearchPost || dataSearchUser) &&
				(
					<div className={styles.dataSearch}>
						{
							dataSearchPost && dataSearchPost.map((data: any, id: number) => 
								<a key={id} className='padding-h-4 flex' href={"/gallery/" + data.post.id}>
									<div className={styles.dataSearch_img}>
										<Image 
											width={40}
											height={40}
											alt={data.post.title}
											src={
												data.medias.length > 1 ?
												"https://f005.backblazeb2.com/file/PnP-Bucket/"+ data.medias[0].fileName
												: "/image/noimage.jpg"
											}
										></Image>
									</div>
									<div className='padding-w-8'>
										<div>{data.post.title}</div>
										<div className={styles.dataSearch_user + " flex"}>
											<div>{data.user.lastName} {data.user.firstName}</div>
											<div className={styles.dataSearch_user_dot}>.</div>
											<div>{TimeConvent(data.post.iat)}</div>
										</div>
										
									</div>

								</a>
							)
						}
						{
							(dataSearchPost && dataSearchUser) && 
							<div className='endtag'></div>
						}
						{
							dataSearchUser?.length > 0 && dataSearchUser.map((data:any, id: number) => 
								<a key={id} className='padding-h-4 flex' href={"/user/" + data.id}>
									<div className={styles.dataSearch_avatar}>
										{data.lastName && data.lastName[0]}
									</div>
									<div className='padding-w-8'>
										<div>{data.firstName} {data.lastName}</div>
										<div className={styles.dataSearch_user + " flex"}>
											<div>{data.email}</div>
										</div>
									</div>
								</a>
							)
						}
						<a className='padding-h-4 flex'  href={"/search?q="+ debouncedSearchTerm}>
							<div className={styles.dataSearch_iconSearch}>
								<SearchIcon />
							</div>
							<div className='flex flex_center padding-w-8'>Search for {debouncedSearchTerm}</div>
						</a>
					</div>
				)
			}
		</form>
	)
}


export { Form, FormUpload, FormSeach} 

