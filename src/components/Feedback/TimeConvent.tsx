import React from 'react'
import styles from '../Feedback/notification.module.scss'
export default function TimeConvent(timeText:string) {


  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];


  let dateTime = ""
  if (timeText) {
    const dateObject = new Date(timeText);
    const vietnamOffset = 7 * 60 * 60 * 1000; 
    const vietnamTime = new Date(dateObject.getTime() + vietnamOffset);
    const day = vietnamTime.getDate();
    const month = vietnamTime.getMonth() + 1; 
    const year = vietnamTime.getFullYear();
    const hours = vietnamTime.getHours();
    const minutes = vietnamTime.getMinutes();
    const seconds = vietnamTime.getSeconds();

    const currentDate = new Date();
    const currentDay = currentDate.getDate();
    const currentMonth = currentDate.getMonth() + 1; 
    const currentYear = currentDate.getFullYear();
    const currentHours = currentDate.getHours();
    const currentMinutes = currentDate.getMinutes();
    const currentSeconds = currentDate.getSeconds();
    
    if (currentMonth - month != 0) dateTime = `${day} ${monthNames[month]}, ${year}`
    else if (currentDay - day != 0) dateTime = `${currentDay - day} days ago`
    else if ((currentHours - hours) != 0) dateTime = `${currentHours - hours} h`
    else if ((currentMinutes - minutes) != 0) dateTime = `${currentMinutes - minutes} minute`
    else dateTime = "Just now"
  } else {
    dateTime = "Just now"
  }

  return dateTime
}