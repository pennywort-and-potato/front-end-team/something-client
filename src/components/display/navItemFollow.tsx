import React, { useEffect, useState } from 'react'
import styles from '../display/navItemFollow.module.scss'
import { getFollower, getFollowUser } from '@/api/follow'
import { useLocalStorage } from 'usehooks-ts'
import { useSelector } from 'react-redux'
import { RootState } from '@/store'
import { Modal } from 'antd'
import { follow,unFollow } from '@/api/follow' 



const ListUserFollow = (props:any) => {
	const { user, dataFl, dataFlus } = props
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const [follower, setFollower] = useState(dataFl)
	const [followUser, setFollowUser] = useState(dataFlus)
	const [dataUser, setDataUser] = useState(user)

	const handleFollow = (id:any) => {
		follow(jwt,id).then(data => {
			setFollower([...follower,data])
			setDataUser([...follower,data])
		})
	}
	const handleUnFollow = (id:any) => {
		unFollow(jwt,id).then(data => {
			setFollower(follower.filter((userFl:any) => userFl.id != id))
			setDataUser(follower.filter((userFl:any) => userFl.id != id))
		})
	}

	return (
		<div className={styles.modal_item}>
			{
				user.length > 0 ? dataUser.map((userFollow:any,id:number) => 
					<div key={id} className={styles.modal_item_user+ " flex flex_justify"}>
						<div className='flex'>
							<div className={styles.modal_item_user_logo}>{userFollow.lastName[0]}</div>
							<div className={styles.modal_item_user_infor}>
								<div className={styles.modal_item_user_infor_name}>{userFollow.firstName}  {userFollow.lastName}</div>
								<div className={styles.modal_item_user_infor_email}>{userFollow.email}</div>
							</div>
						</div>
						<div className={styles.button}>
							{
								(follower.find((userFl:any) => userFl.id == userFollow.id)  &&
								followUser.find((userFl:any) => userFl.id == userFollow.id)) ?
									<div className={styles.buttonFriend} onClick={() => handleUnFollow(userFollow.id)}>Friend</div>
								: (
									follower.find((userFl:any) => userFl.id == userFollow.id) ?
									<div className={styles.buttonFollowing} onClick={() => handleUnFollow(userFollow.id)}>Following</div>
									: (
										followUser.find((userFl:any) => userFl.id == userFollow.id) ?
										 <div className={styles.followBack} onClick={() => handleFollow(userFollow.id)}>follow back</div>
										: <div onClick={() => handleFollow(userFollow.id)}>Following</div>
									)
								)

							}
						</div>
					</div>
				)
				: <div>Bạn chưa follow ai</div>
			}
		</div>
	)
}


export default function NavItemFollow(props:any) {
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const user  = useSelector((state: RootState) => state.user)
	const { data } = props
	const [follower, setFollower] = useState([])
	const [followUser, setFollowUser] = useState([])
	const [isModalFollow, setIsModalFollow] = useState(false);
	const [modalTap, setModalTap] = useState(1);
	const userLiked = data.reduce((total:any,item:any) => {
		return total + item.post.like
	},0) 
	useEffect(()=> {
		getFollowUser(jwt).then((data) => {
			setFollower(data)
		})
		getFollower(jwt).then((data)=> {
			setFollowUser(data)
		})
	},[user])
	const showModalFollow = (idTap:number) => {
		setIsModalFollow(true);
		setModalTap(idTap)
	};

	const handleOk = () => {
	setIsModalFollow(false);
	};

	const handleCancel = () => {
	setIsModalFollow(false);
	};
	let Arrfriend = follower.filter((userFl:any) => 
			followUser.some((userFluser:any) => userFluser.id == userFl.id)
		)

	return (
		<div className={styles.follow + " flex flex_center"} >
			<div className={styles.follow_item} onClick={() => showModalFollow(1)}>
				<div className={styles.follow_item_total}>{follower && follower.length}</div>
				<div className={styles.follow_item_text}>Following</div>
			</div>
			<div className={styles.follow_item} onClick={() => showModalFollow(2)}>
				<div className={styles.follow_item_total}>{followUser && followUser.length}</div>
				<div className={styles.follow_item_text}>Follower</div>
			</div>
			<div className={styles.follow_item}>
				<div className={styles.follow_item_total}>{userLiked}</div>
				<div className={styles.follow_item_text}>Like</div>
			</div>
			<Modal width={350} footer={null} open={isModalFollow} onOk={handleOk} onCancel={handleCancel}>
				<div className={styles.modalNav + " flex flex_center"}>
					<div className={modalTap == 1 ? styles.modalNav_item_active : styles.modalNav_item} onClick={() => setModalTap(1)}>Following {follower && follower.length}</div>
					<div className={modalTap == 2 ? styles.modalNav_item_active : styles.modalNav_item} onClick={() => setModalTap(2)}>Follower {followUser && followUser.length}</div>
					<div className={modalTap == 3 ? styles.modalNav_item_active : styles.modalNav_item} onClick={() => setModalTap(3)}>Friend {Arrfriend && Arrfriend.length}</div>
				</div>
				{
					modalTap == 1 &&
					<ListUserFollow user={follower} dataFl={follower} dataFlus={followUser}/>
				}
				{
					modalTap == 2 &&
					<ListUserFollow user={followUser} dataFl={follower} dataFlus={followUser}/>
				}
				{
					modalTap == 3 &&
					<ListUserFollow user={Arrfriend} dataFl={follower} dataFlus={followUser}/>
				}
				
			</Modal>
		</div>
	)
}