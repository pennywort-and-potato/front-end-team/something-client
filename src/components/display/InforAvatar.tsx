import React, { useEffect, useState } from 'react'
import styles from '../display/inforAvatar.module.scss'
import Link from 'next/link'
import { follow, getFollowUser, unFollow } from '@/api/follow'
import { useLocalStorage } from 'usehooks-ts'
import { useSelector } from 'react-redux'
import { RootState } from '@/store'
import TimeConvent from '../Feedback/TimeConvent'

export default function InforAvatar(props:any) {
	
	const {dataUser, dataUserLogin ,date, listUserFolow} = props
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const user  = useSelector((state: RootState) => state.user)

	const [ followText, setFollowText ] = useState<any>();
	let dateTime = TimeConvent(date)
	useEffect(() => {
		if (listUserFolow) {
			if(listUserFolow.find((userFolow:any) => userFolow.id == dataUser.id)) {
				setFollowText(true)
			} else {
				setFollowText(false)
			}
		} else {
			setFollowText(false)
		}
	},[jwt])

	const handleFollow = async () => {
		await follow(jwt,dataUser.id).then(data => {
			setFollowText(true)
		})
	}

	const handleUnFollow = async () => {
		await unFollow(jwt,dataUser.id).then(data => {
			setFollowText(false)
		})
	}
    return (
        <div  className={styles.inforAvatar}>
			{
				dataUser &&
				<div className='flex'>
					<Link href={dataUser.id == dataUserLogin.id ? "/user" : `/user/${dataUser.id}`} className='flex'>
						<div className={styles.avatar}>
							{dataUser.lastName[0]}
						</div>
						<div className={styles.infor}>
							<div className={styles.inforName}>
								<div>{dataUser.firstName} {dataUser.lastName}</div>
								
								
							</div>
							<div className={styles.inforView}>
								{dateTime}
							</div>
						</div>
					</Link>
					{
						jwt &&
						<div className='flex'>
							<div className={styles.infor_dot}>.</div>
							{
								dataUser.id != dataUserLogin.id && follow != undefined &&
								(
									followText ?
									<div className={styles.infor_follow} style={{color: "#ababab"}} onClick={handleUnFollow}>Followed</div>
									:
									<div className={styles.infor_follow} onClick={handleFollow}>Follow</div>
								)
							}
							

						</div>
					}
					
				</div>
				
				
			}
			
			
        </div>
    )
}