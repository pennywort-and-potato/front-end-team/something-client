import { LikePost, UnLikePost } from '@/api/post'
import { RootState } from '@/store';
import { CommentOutlined, EyeOutlined, LikeOutlined, SaveOutlined } from '@ant-design/icons'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useLocalStorage } from 'usehooks-ts';
import styles from '../display/navItemPost.module.scss'
import { Dropdown, MenuProps, Space } from 'antd';
import { AddBookmark, UnBookmark } from '@/api/bookmark';
import IconSave from '@/components/svg/save.svg'
import IconSaved from '@/components/svg/saved.svg'
import { useRouter } from 'next/navigation'


export default function NavItemPost(props:any) {
	
	let { data, allBookmark } = props
    const user  = useSelector((state: RootState) => state.user)
    const [ likePost, setLikePost ] = useState<any>();
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '');
    const router = useRouter()

	const [ dataBookmark, setDataBookmark] = useState<any>(allBookmark);
    useEffect(() => {
		if(data.likeList) setLikePost({ 
			quantity: data.like, 
			faliked : data.likeList.find((userLike:any) =>  userLike.id == user.id) == undefined ? false : true
		})
	},[jwt])

    const handleUnLikePost = () => {
		if(jwt) UnLikePost(data.id, jwt).then(dataLike => {
			setLikePost({
				quantity: dataLike.post.like,
				faliked: dataLike.likeList.find((userLike:any) =>  userLike.id == user.id) == undefined ? false : true
			})
		})
	}

    const handleLikePost = () => {
		if(jwt) LikePost(data.id, jwt).then(dataLike => {
			setLikePost({
				quantity: dataLike.post.like,
				faliked: dataLike.likeList.find((userLike:any) =>  userLike.id == user.id) == undefined ? false : true
			})
		})
		else router.push("/login")
	}

    const items: MenuProps['items'] = data?.likeList ? data.likeList.reduce((arr :any,item :any) => {
        let itemNameLike = {
            label: (
                <div>{item.firstName} {item.lastName}</div>
              ),
              key: item.id,
        }
        return [...arr, itemNameLike]
    },[]) : []   
    const handleSave = () => {
        if(jwt) {
            AddBookmark(data.id,jwt).then(data => {
                setDataBookmark(data)
            })
        } else router.push("/login")
    }
    const handleUnSave = () => {
        if(jwt) {
            UnBookmark(data.id,jwt).then(data => {
                setDataBookmark(data)
            })
        } else router.push("/login")
    }
	return (
        <div>
            <div className="flex flex_justify">
                {data.likeList && (
                    <div className='flex'>
                        {
                            likePost?.faliked ? (
                                <Dropdown menu={{ items }} >
                                    <a onClick={(e) => e.preventDefault()}>
                                        <Space>
                                            <div className={styles.infor_item} onClick={handleUnLikePost}>
                                                <LikeOutlined className={styles.liked}/>
                                                <div className={styles.infor_item_content + " padding-w-8"}>{likePost.quantity}</div>
                                            </div>
                                        </Space>
                                    </a>
                                </Dropdown>
                               
                            ):(
                                <Dropdown menu={{ items }} >
                                    <a onClick={(e) => e.preventDefault()}>
                                        <Space>
                                            <div className={styles.infor_item}  onClick={handleLikePost}>
                                                <LikeOutlined />
                                                <div className={styles.infor_item_content + " padding-w-8"}>{likePost?.quantity || 0}</div>
                                            </div>
                                        </Space>
                                    </a>
                                </Dropdown>
                                
                            )
                        }
                        <div className={styles.infor_item} >  
                            <EyeOutlined />
                            <div className={styles.infor_item_content + " padding-w-8"}>{data.view}</div>
                        </div>
                    </div>
                )}
                
                <div className='flex'>
                    {
                        dataBookmark?.posts.find((postData:any) => postData.post.id == data.id) ?
                        <div className={styles.infor_item} onClick={handleUnSave}>
                            <IconSaved/>
                        </div>
                            :
                        <div className={styles.infor_item} onClick={handleSave}>  
                            <IconSave/>
                        </div>  
                    }
                    
                    
                </div>
                
            </div>
        </div>
		
		
	)
}