import React from 'react'
import styles from '../display/listImage.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import ViewYey from '@/components/svg/viewYey.svg'
import HidenYey from '@/components/svg/slashYey.svg'

const ImageLink = (props :any) => {
	const { width, postId, postLink, classAdd} = props
	return	(
		<Link href={"/gallery/" + postId} style={{width: width}}>
			<Image 
				className={"imagePost " +classAdd}
				alt="description of image"
				height={300}
				width={300}
				src={postLink && postLink != "undefined"?  "https://f005.backblazeb2.com/file/PnP-Bucket/" + postLink : "/image/noimage.jpg"}
			/>
		</Link>
	)
}
export default function ListImage(props:any) {
	
	let {data, showImg, handleShowImage } = props
	return (
		
		<div className='listImage_item_image'>
			{data.listImage.length == 0 && 
				
				<Link href={"/gallery/" + data.id}>
					<Image 
						className={"imagePost"}
						alt="description of image"
						height={300}
						width={300}
						src="/image/noimage.jpg"
					/>
				</Link>
			
			}
			{
				data.listImage.length == 1 && 
				<ImageLink postLink={data.image} classAdd="" postId={data.id} width="100%"/>
			}
			{
				data.listImage.length == 2 && 
				<div className='flex grupImage_item'>
					<ImageLink postLink={data.listImage[0].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
					<ImageLink postLink={data.listImage[1].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
				</div>
				
			}
			
			{
				(data.listImage.length == 3) &&
				<div>
					<div className='grupImage_item'>
						<ImageLink postLink={data.listImage[0].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
					</div>
					<div  className='flex grupImage_item'>
						<ImageLink postLink={data.listImage[1].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
						<ImageLink postLink={data.listImage[2].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
					</div>
					
				</div>
			}
			{
				(data.listImage.length > 3) &&
				<div>
					<div className='grupImage_item'>
					<ImageLink postLink={data.listImage[0].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
					</div>
					<div  className='flex grupImage_item'>
						<ImageLink postLink={data.listImage[1].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
						<ImageLink postLink={data.listImage[2].fileName} classAdd="boxImage" postId={data.id} width="100%"/>
						<div className={styles.grup_imageNext}>
							{data.listImage.length - 3 + "+"} 
						</div>
					</div>
					
				</div>
			}
			
			<Link href={"/gallery/" + data.id}>
				{	
					(data.nsfw && showImg) && (<div  className='blur'></div>)
				}
			</Link>
			

			{(data.nsfw) && 
			(<div className="listImage_item_view" onClick={handleShowImage}>
				{showImg ?  <HidenYey/> : <ViewYey /> }
			</div>)}
	</div>
	)
}