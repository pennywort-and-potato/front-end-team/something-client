import { RootState } from '@/store';
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useLocalStorage } from 'usehooks-ts';
import { GetPostComment, Comment, DeleteComment, LikeComment, UnLikeComment } from '@/api/comment';
import styles from '../display/commentPost.module.scss'
import { CloseOutlined, HeartOutlined, SendOutlined } from '@ant-design/icons';
import { useRouter } from 'next/navigation'
import Link from 'next/link';
import TimeConvent from '../Feedback/TimeConvent';

export default function CommentPost(props:any) {
	const{ idPost } = props
	const [jwt, setJwt] = useLocalStorage("user_jwt", "");
	const user = useSelector((state: RootState) => state.user);
	const [comment , setCommment] = useState<any>([])
	const [commentText, setCommentText] = useState("");
	const router = useRouter()

	useEffect(()=> {
		GetPostComment(idPost,1,jwt).then(data => {
			setCommment(data.data)
		})
	},[user])

	const handleComment = async (e:any) => {
		e.preventDefault()
		const formData = new FormData(e.target)
		const valueBody = formData.get('body');
		if(jwt) await Comment(idPost,valueBody, jwt).then((dataComment:any)=> {
			dataComment.comment.iat = ''
			setCommment([dataComment,...comment])
			setCommentText("");
			
		})
		else router.push("/login")
	}

	const handleDelete = async (id:any) => {
		if(jwt) await DeleteComment(id,jwt).then(data => {
			console.log(data)
			setCommment(comment.filter((item:any) => item.comment.id != id))
		})
	}
	
	const handleLike = (id:any) => {
		if(jwt) LikeComment(id,jwt).then(data => {
			setCommment(
				comment.map((item:any) => {
					return item.comment.id == data.comment.id ?  data : item
				})
			)
		})
		else router.push("/login")
	}
	const handleUnLike = (id:any) => {
		if(jwt) UnLikeComment(id,jwt).then(data => {
			setCommment(
				comment.map((item:any) => {
					return item.comment.id == data.comment.id ?  data : item
				})
			) 
		})
		else router.push("/login")
	}
	const handleChangeText = (event:any) => {
		setCommentText(event.target.value);
	}

	return (
		
		<div  >
			{comment.length != 0 &&  <div className='padding-h-10'>{comment.length} comment</div>}
			 <div className={styles.comment}> 
				{
					comment.map((itemCm: any,id: number)=> {
						let dateTime = TimeConvent(itemCm.comment.iat)
						return (
							<div key={id} className={styles.comment_body}>
	
								<Link href={"/user/" + itemCm.user.id} className={styles.comment_body_user}>
									<div className={styles.comment_body_user_avatar}>{itemCm.user.lastName[0]}</div>
									<div className={styles.comment_body_user_infor}>
										<div className={styles.comment_body_user_infor_name}>{itemCm.user.firstName} {itemCm.user.lastName}</div>
										<div className={styles.comment_body_user_infor_comment}>{itemCm.comment.body}</div>
										<div className={styles.comment_body_user_infor_footer}>
											<div className={styles.comment_body_user_infor_footer_time}>
												{dateTime}
											</div>
											
											<div className={styles.comment_body_user_infor_footer_like}>
												{itemCm.comment.like} like
											</div>
											<div className={styles.comment_body_user_infor_footer_delete}>
												{
													user.id == itemCm.user.id &&
													<div className={styles.comment_body_delete} onClick={() => handleDelete(itemCm.comment.id)}>
														Delete
													</div>
												}
											</div>
										</div>
									</div>
								</Link>
								<div className='flex'>
									{/* {
										user.id == itemCm.user.id &&
										<div className={styles.comment_body_delete} onClick={() => handleDelete(itemCm.comment.id)}>
											<CloseOutlined />
										</div>
									} */}
									{
										itemCm.likeList.find((itemLike: any) => itemLike.id == user.id) ?
										<div className={styles.comment_body_unLike}>
											<HeartOutlined onClick={() => handleUnLike(itemCm.comment.id)}/>
										</div>
										:
										<div className={styles.comment_body_like}>
											<HeartOutlined onClick={() => handleLike(itemCm.comment.id)}/>
										</div>
										
									}
									
								</div>
								
							</div>
						)
					})
				}
			</div>

			<div className={styles.form}>
				<form  className={styles.form_body} onSubmit={handleComment}>
					<div  className={styles.form_body_box}>
					  <input placeholder="Thêm bình luận…" value={commentText} onChange={handleChangeText} type="text" name="body" className={styles.form_body_box_input}/>
					  <button className={styles.form_body_box_button}><SendOutlined /></button>
					</div>
				</form>
			</div>
			
		</div>
	)
}