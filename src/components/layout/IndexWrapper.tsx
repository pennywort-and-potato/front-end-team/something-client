'use client'

import React, { useEffect, useState } from 'react';
import { MediaQuatyCol } from '../event/Media';

import PostItem from './PostItem';

import LazyItem from '../event/LazyItem';
import { getFollowUser } from '@/api/follow';
import { useLocalStorage } from 'usehooks-ts';
import { useSelector } from 'react-redux';
import { RootState } from '@/store';
import { GetAllBookmark } from '@/api/bookmark';
import LoadBoxContent from '../loading/LoadBoxContent';
function IndexWrapper(props:any) {
	const  { data, deletePost, followUserData, allBookmark }  =  props
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '');
	const user  = useSelector((state: RootState) => state.user)

	//const nuberCol: any = MediaQuatyCol(300) 
	let itemContenImg: any = data  ? data : []
	return (
		
		
		<>  
				<div className="Index_container">
					{
						itemContenImg?.length != 0 	? 
						itemContenImg.map((data:any,id:number) => 
						<LazyItem loading={<LoadBoxContent/>} key={id} item={(<PostItem allBookmark={allBookmark} itemDelete={deletePost} key={id} data={data} widthFull={true} listUserFolow={followUserData}/>)} />)
						: <div className='listImage_container_empty  flex flex_center'>It is empty in here</div>
					}
				</div>
		</>
	   
		
	)
}


export default IndexWrapper;

function setFollowText(arg0: boolean) {
	throw new Error('Function not implemented.');
}
