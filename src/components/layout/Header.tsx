'use client';

import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import Dropdown from '../Navigation/Dropdown';
import Upload from '@/components/svg/upLoad.svg';
import Plus from '@/components/svg/plus.svg';
import { useSelector } from 'react-redux';
import { RootState } from '@/store';
import { useLocalStorage } from 'usehooks-ts';
import { FormSeach } from '../dataEntry/Form';
import { useRouter } from 'next/navigation';
import { Button, Checkbox, Form, Input, Modal } from 'antd';
import UploadPost from './UploadPost';
import LogoIndex from '@/assets/logo.png';
import { MediaQuery } from '../event/Media';

const inputs = [
  {
    id: 1,
    name: 'search',
    type: 'text',
    placeholder: 'Quick Search',
    label: '',
    errorMessage: 'lo áda',
    require: 'true',
  },
];

export default function Header() {
  const [jwt, setJwt] = useLocalStorage('user_jwt', '');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const router = useRouter();
  const logout = (data: any) => {
    setJwt('');
    router.push(`/`);
  };
  const [ theme, setTheme ] = useLocalStorage('Theme', true)
  const themeMode = () => {
    setTheme(!theme)
    theme ? document.body.classList.add('light') : document.body.classList.remove ('light')
  }
  const itemsUser = [
    {
      key: '1',
      label: <Link href='/user'>Profile</Link>,
    },
    {
      key: '2',
      label: <div onClick={() => themeMode()}>{theme? "Light theme" : "Dark theme"}</div>,
    },
    {
      key: '3',
      label: <Link href='/'>Like</Link>,
    },
    {
      key: '4',
      label: <div onClick={logout}>Logout</div>,
    },
  ];
  const user = useSelector((state: RootState) => state.user);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const data = Object.fromEntries(formData.entries());
    location.href = "/search?q=" + data.search
  };

  const showModalUpload = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div className='header'>
      <div className='container flex flex_justify'>
        <div className='header_box header_box_left flex flex_center'>
          <div className='header_box_item'>
            <Link href='/'>
              <div className='logoImage'></div>
            </Link>
          </div>
        </div>
        {
          !MediaQuery(678) &&
          <div className='header_box flex flex_center'>
            <div className='header_box_item'>
              <FormSeach 
                handleSubmit={handleSubmit}
                inputs={inputs}
                buttonSeach={true}
                setHidePost={(check:boolean) => setIsModalOpen(check)}
              />
            </div>
            
          </div>
        }
       
        <div className='header_box header_box_right flex flex_center'>
          {user.username && (
            <div className='header_box_item'>
              <div
                onClick={showModalUpload}
                className='button flex flex_center header_box_upload'
              >
                <Plus className='icon' />
              </div>
              <UploadPost
                isModalOpen={isModalOpen}
                handleCancelPost={handleCancel}
              />
            </div>
          )}
          {user.username ? (
            <Dropdown
              menu={itemsUser}
              className='dropdown_list'
            >
              <div className='button header_box_item_user'>
                {user.lastName[0]}
              </div>
            </Dropdown>
          ) : (
            <div className='header_box_item'>
              <Link
                className='button'
                href='/login'
              >
                Sign In
              </Link>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
