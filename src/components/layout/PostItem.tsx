'use client'

import React, { use, useEffect, useState } from 'react'
import Image from 'next/image';
import Link from 'next/link';
import ViewYey from '@/components/svg/viewYey.svg'
import HidenYey from '@/components/svg/slashYey.svg'
import { CommentOutlined, DeleteOutlined, EllipsisOutlined, EyeOutlined, LikeOutlined, SaveOutlined } from '@ant-design/icons';
import {  useSelector } from 'react-redux'
import { RootState } from '@/store';


import Avatar from '../Navigation/Avatar';
import { Modal } from 'antd';
import { deletePost, LikePost, UnLikePost } from '@/api/post';
import { useLocalStorage } from 'usehooks-ts';
import InforAvatar from '../display/InforAvatar';
import ListImage from '../display/ListImage';
import { getFollowUser } from '@/api/follow';
import NavItemPost from '../display/navItemPost';
import CommentPost from '../display/commentPost';

 
function PostItem(props : any) {
	const user  = useSelector((state: RootState) => state.user)
	const { data , itemDelete, widthFull, listUserFolow, allBookmark} = props
	const [ showImg, setShowImg] = useState(true)
	
	const [ isModalOpen, setIsModalOpen] = useState(false);
	const [ deletePostShow , setDeletePostShow ] = useState(true);
	const [ likePost, setLikePost ] = useState<any>();
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '');
	const [ follow, setFollow ] = useState<any>();
	useEffect(() => {
		if(data.likeList) setLikePost({ 
			quantity: data.like, 
			faliked : data.likeList.find((userLike:any) =>  userLike.id == user.id) == undefined ? false : true
		})
	},[user])

	let itemsSettingImg = [
		{
			key: '2',
			label: (
				<Link href="/">Like</Link>
			),
		},
	]
	
	itemsSettingImg = itemsSettingImg.filter(el => el.key != "3" || data.nsfw)

	const showModal = () => {
		setIsModalOpen(true);
	};

	const handleOk = () => {
		deletePost(data.id, jwt).then(data=> {
			setIsModalOpen(false);
			setDeletePostShow(false)
		})

	};
	const handleCancel = () => {
		setIsModalOpen(false);
	};
	

	const handleShowImage = () => setShowImg(!showImg) 
	return (
		<div className={widthFull? "itemIndex" : ""}>
			{
				deletePostShow  &&  (
					<div className="listImage_item">
						{
							data.user &&
							<div className='listImage_item_user flex flex_justify'>
								<InforAvatar dataUser={data.user} dataUserLogin={user} date={data.iat} listUserFolow={listUserFolow}/>
								<div className='listImage_item_user_follow'>
									<EllipsisOutlined />
								</div>
							</div>
						}
						
						{
							widthFull &&
							<div className='listImage_item_tiltel'>
								<h4>{data.title}</h4>
								<div className='listImage_item_tiltel_body'>{data.body}</div>
							</div>
						}
						{	
							data.listImage	?
							<ListImage data={data} showImg={showImg} handleShowImage={handleShowImage}/>
							: 
							<Link href={"/gallery/" + data.id}>
								<Image 
									className={"imagePost"}
									alt="description of image"
									height={300}
									width={300}
									src={data.image ? "https://f005.backblazeb2.com/file/PnP-Bucket/" + data.image : "/image/noimage.jpg"}
								/>
							</Link>
						}
						
						
						
						
						<div 
							style={{borderRadius: "0 0 4px 4px"}}
							className="listImage_item_infor">
							{	
								!widthFull &&
								<div className="listImage_item_infor-name flex flex_justify">
									<div>{data.title}</div>
								</div>
							}
							
							<NavItemPost data={data} allBookmark={allBookmark}/>
						</div>
						{
							widthFull &&
							<CommentPost idPost={data.id}/>
						}
						
						{
							itemDelete && (
								<div className='listImage_item_delete'>
									<div onClick={() => setIsModalOpen(true)}>
										<DeleteOutlined />
									</div>
									
									<Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
										<DeleteOutlined style={{fontSize:"20px",color:"red"}}/>
										<div>Bạn có chắc chắn muốn xóa Post này không?</div>
										<div>Bài post sẽ không thể khôi phục lại</div>
									</Modal>
								</div>
								
							)
						}
					</div>
				)
			}
		</div>
		
	)
}

export default PostItem