'use client'

import { follow, getFollowUser, getFollower, unFollow } from '@/api/follow';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useLocalStorage } from 'usehooks-ts';
function ListUser(props:any) {
	const {data, propsFollow, size } = props
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const [user, setUser] = useState<any>([]);

	const [follower, setFollower] = useState([])
	const [followUser, setFollowUser] = useState([])
	useEffect(()=> {
		getFollowUser(jwt).then((data) => {
			setFollower(data)
			
		})
		getFollower(jwt).then((data)=> {
			setFollowUser(data)
		})
	},[user])

	let handleFollow = (idUser: number) => {
		follow(jwt,idUser).then((data: any) => {
			let arrDataUserFollow: any = [...follower, data]
			setFollower(arrDataUserFollow)
		})
	}

	let handleUnFollow = (idUser: number) => {
		unFollow(jwt,idUser).then((data: any) => {
			setFollower(follower.filter((data:any) => data.id != idUser))
		})
	}
	return (
		<>
			{
				propsFollow && <h3 className='margin-t-12 '>People</h3>
			}
			{
				data?.map((userFollow:any,id:number) => {
					
					let followButton = 1
					let checkFollow = follower.find((data:any) => data.id == userFollow.id)
					let checkUserFollow = followUser.find((data:any) => data.id == userFollow.id)
					if(checkFollow && checkUserFollow) {
						followButton = 2
					}else if(checkFollow) {
						followButton = 3
					}else if(checkUserFollow) {
						followButton = 4
					}
					return (
						<div  key={id} className='flex flex_justify'>
							<Link href={"/user/"+ userFollow.id} className={'flex listUser ' + size}>
								<div className='listUser_avatar' >{userFollow.firstName[0]}</div>
								<div >
									<div className='listUser_inforName'>{userFollow.firstName}  {userFollow.lastName}</div>
									<div className='listUser_inforView'>{userFollow.email}</div>
								</div>
							</Link>
							{
								propsFollow &&
								<div className='flex flex_center'>
									{
										followButton == 1 && 
										<div onClick={() => handleFollow(userFollow.id)} className='listUser_follow'>
											Follow
										</div>
									}
									{
										followButton == 2 && 
										<div onClick={() => handleUnFollow(userFollow.id)} className='listUser_follow'>
											Friend
										</div>
									}
									{
										followButton == 3 && 
										<div onClick={() => handleUnFollow(userFollow.id)} className='listUser_follow'>
											Following
										</div>
									}
									{
										followButton == 4 && 
										<div onClick={() => handleFollow(userFollow.id)} className='listUser_follow'>
											follow back
										</div>
									}
								</div>
							}
							
						</div>
					)
				})
			}
		</>	
	)
}


export default ListUser;