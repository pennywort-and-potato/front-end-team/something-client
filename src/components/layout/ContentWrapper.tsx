'use client'

import React, { useEffect } from 'react';
import Image from 'next/image';
import { MediaQuatyCol } from '../event/Media';
import MoreVertica  from '@/components/svg/moreVertica.svg'
import Dropdown from '../Navigation/Dropdown';
import PostItem from './PostItem';
import { Upload } from 'antd';
import LazyItem from '../event/LazyItem';
function ContentWrapper(props:any) {




	const nuberCol: any = MediaQuatyCol(300) 

	const  { data, deletePost,allBookmark }  =  props
	let itemContenImg = data  ? data : []

	return (
		
		nuberCol ?
		<>  
				<div 
					style={{maxWidth: `${ nuberCol * 400 + nuberCol * 16}px`,
							margin: "auto"}} 
					className="flex flex_center flex_wrap listImage_container"
					>
					{
						itemContenImg?.length != 0 	? 
						itemContenImg.map((data:any,id:number) => 
						<LazyItem loading={true} key={id} item={(<PostItem allBookmark={allBookmark} itemDelete={deletePost} key={id} data={data} />)} />)
						: <div className='listImage_container_empty  flex flex_center'>It is empty in here</div>
					}
				</div>
		</>
	   
		:
		<div className='listImage_none'></div>
		
	)
}


export default ContentWrapper;