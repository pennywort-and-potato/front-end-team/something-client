'use client'
import { PlusOutlined, UploadOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, Modal, Upload, message } from 'antd'
import { RcFile, UploadFile, UploadProps } from 'antd/es/upload';
import React, { useEffect, useState } from 'react'
import { uploadUrl } from "@/utils/common";
import { useLocalStorage } from 'usehooks-ts'

import axios from 'axios';
import { addContentoPost, createPost } from '@/api/post';
import { useRouter } from 'next/navigation';

export default function  UploadPost(props:any) {

	const { isModalOpen, handleCancelPost } = props;
	const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
	const router = useRouter()
	const [showCreatePost, setShowCreatePost] = useState<boolean>(false);
	const [messageApi, contextHolder] = message.useMessage();

	useEffect(()=> {
		setShowCreatePost(isModalOpen)
	},[isModalOpen])

	const onFinish = async (values: any) => {

		const formDataCreatePost = new FormData();
		formDataCreatePost.append('title', values.title);
		formDataCreatePost.append('body', values.body);
		formDataCreatePost.append('nsfw', values.nsfw ? values.nsfw : false);
		formDataCreatePost.append('isPrivate ', values.isPrivate ? values.isPrivate : false);
			const formDataAddConten = new FormData();
			let checkImageDone = false
			fileList.map((item:any) => {
				if (item.status == 'done'){
					formDataAddConten.append('mediaId', item.uid);
					checkImageDone = true
				} 
			})

		if (checkImageDone) {
			let idPost

			await createPost(formDataCreatePost, jwt).then(res => {
				if(res) {
					//console.log("post thanh công",res.post.id)
					idPost = res.post.id
				}
			})
			if (idPost){

				formDataAddConten.append('postId', `${idPost}`);
				await addContentoPost(formDataAddConten, jwt).then(res => {
					if(res) {
						//console.log("post to image")
						location.href = `/gallery/${res.post.id}`
					} 
				})
			} else {
				messageApi.open({
					type: 'error',
					content: 'Post bị lỗi',
				});
			}
			
		}
		else {
			messageApi.open({
				type: 'error',
				content: 'Bạn chưa thêm ảnh vào post',
			});
		}


	};
	
	const [fileList, setFileList] = useState<UploadFile[]>([]);
	const handleUpload = async ({ file, onSuccess, onError, fileList: newFileList } :any) => {
		const formData = new FormData();

		setFileList([...fileList,
			{
				uid: file.uid,
				name: 'response.data.fileName',
				status: 'uploading',
			}
		]);
		formData.append('image', file);
		try {
			const response = await axios.put('https://www.pnp.name.vn/uploader/uploadImage/single', formData,{
				headers: {
					'Authorization': jwt
				} 
			});
			if (response){
				setFileList([...fileList,
					{
						uid: response.data.data.fileId,
						name: 'response.data.fileName',
						status: 'done',
						url: "https://f005.backblazeb2.com/file/PnP-Bucket/" + response.data.data.fileName       
					}
				]);
			}

		} catch (error) {
			setFileList([...fileList,
				{
					uid: file.uid,
					name: 'image.png',
					status: 'error',
				}
			]);
		}
	};
	
	const handleRemove = (file:any) => {
		const updatedFileList = fileList.filter((item) => item.uid !== file.uid);
		setFileList(updatedFileList);
	};

	const onPreview = () => {}

	const uploadButton = (
		<div>
			<PlusOutlined />
			<div style={{ marginTop: 8 }}>Upload</div>
		</div>
	);
	
	return (
		<div className='uploadPost'>
				{contextHolder}
				 <div>
						<Modal 
								open={showCreatePost} 
								footer={null} 
								width={400}		
								onCancel={handleCancelPost}
							>
								<h2 className='flex flex_center'>CREATE POST</h2>
								<Form
										name="basic"
										labelCol={{ span: 24 }}
										wrapperCol={{ span: 24 }}
										style={{ maxWidth: 600 }}
										initialValues={{ remember: true }}
										onFinish={onFinish}
										autoComplete="off"
										className='formUplpad'
								>
										<Form.Item
											label="Title"
											name="title"
											rules={[{ required: true, message: 'Please input your title!' }]}
											>
											<Input />
										</Form.Item>

										<Form.Item
											label="Body"
											name="body"
											rules={[{ required: true, message: 'Please input your body!' }]}
											>
											<Input />
										</Form.Item>
										<Form.Item
											label="Image"
											>
											<Upload
												listType="picture-card"
												fileList={fileList}
												onRemove={handleRemove}
												customRequest={handleUpload}
												onPreview={onPreview}
											>
												{fileList.length >= 8 ? null : uploadButton}
											</Upload>
											
										</Form.Item>

										<Form.Item name="nsfw" valuePropName="checked" wrapperCol={{ offset: 1, span: 16 }}>
												<Checkbox defaultChecked={false}>NSFW</Checkbox>
										</Form.Item>
										<Form.Item name="isPrivate" valuePropName="checked" wrapperCol={{ offset: 1, span: 16 }}>
												<Checkbox defaultChecked={false}>Private</Checkbox>
										</Form.Item>

										<Form.Item wrapperCol={{ offset: 0, span: 0 }}>
												<Button className='buttonSubmit' type="primary" htmlType="submit">
													Create
												</Button>
										</Form.Item>
								</Form>
						</Modal>
				</div>
		</div>
		
	)
}
