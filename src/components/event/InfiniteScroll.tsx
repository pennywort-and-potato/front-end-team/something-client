import { useEffect, useRef } from 'react';
import { useInView } from 'react-intersection-observer';

interface InfiniteScrollProps {
  onLoadMore: () => void,
  isload:boolean
}

const InfiniteScroll: React.FC<InfiniteScrollProps> = ({ onLoadMore , isload}) => {
  const loaderRef = useRef<HTMLDivElement>(null);
  const [inViewRef, inView] = useInView();

  useEffect(() => {
    if (inView && isload) {
      onLoadMore();
    }
  }, [inView, isload]);

  return (
    <div ref={inViewRef}>
      <div ref={loaderRef} />
    </div>
  );
};

export default InfiniteScroll;