
import React, { useCallback, useEffect, useState } from 'react';

export const MediaQuatyCol = (width:number) => {
    const nuberColDef = typeof window === undefined ? Math.floor((window.innerWidth - 50)/ width) :  null
    const [nuberCol, setNuberCol] = useState<Number | null>(nuberColDef); 
    
    const handleResize = () =>{
        if( Math.floor((window.innerWidth - 50)/ width) !=  nuberCol) setNuberCol(Math.floor((window.innerWidth - 50) / width))
    }  
    useEffect(() => {
        handleResize()
        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);
    return nuberCol;
    
        
}

export const MediaQuery = (width:number) => {
    const [targetReached, setTargetReached] = useState(false)

    const updateTarget = useCallback((e: any) =>
    {
        if (e.matches) setTargetReached(true)
        else setTargetReached(false)
    }, [])

    useEffect(() =>
    {
        const media = window.matchMedia(`(max-width: ${width}px)`)
        media.addEventListener('change', updateTarget)
        if (media.matches) setTargetReached(true)
        
        return () => media.removeEventListener('change', updateTarget)
    }, [width, updateTarget])

    return targetReached
}
