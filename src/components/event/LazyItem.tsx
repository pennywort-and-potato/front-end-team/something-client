"use client"
import React, { useEffect, useRef, useState } from 'react';
import { useInView } from 'react-intersection-observer';
import Header from '../layout/Header';
import Image from 'next/image';
import LoadBoxContent from '../loading/LoadBoxContent';

interface LazyItemProps {
  item: React.ReactNode,
	loading: React.ReactNode;
}

const LazyItem: React.FC<LazyItemProps> = ({ item, loading }) => {

	const [inViewRef, inView] = useInView();
	const [isLoaded, setIsLoaded] = useState(false);
	const [addCompoment, setAddCompoment] = useState<any>(loading);

	useEffect(() => {
		if (inView && !isLoaded) {
			// console.log(`Loading item: ${item}`);
			setIsLoaded(true);
			setAddCompoment(item)
		}   
	}, [inView, item]);

	return (
		<div ref={inViewRef}>
			{addCompoment}
		</div>
	);
};

export default LazyItem;