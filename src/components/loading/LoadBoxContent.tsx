
import React from 'react'
import styles from '../loading/LoadBox.module.scss'
function LoadBoxContent(props: any) {

  return (
    <div className='Index_container animations-fade-5'>
      <div className="itemIndex">
        <div className='listImage_item'>
          <div className="listImage_item_user flex flex_justify">
            <div className={styles.inforAvatar}>
              <div className="flex">
                <a className="flex" href="/user/">
                  <div className={styles.inforAvatar_avatar }></div>
                  <div className=''>
                    <div className={styles.inforAvatar_inforName + " " + styles.box_loading_title}>
                      <div>....</div>
                    </div>
                    <div className={styles.inforAvatar_inforView + " " + styles.box_loading_content} ></div>
                  </div>
                </a>
              </div>
            </div>
            <div className="listImage_item_user_follow">
              <span role="img" aria-label="ellipsis" className="anticon anticon-ellipsis">
                <svg viewBox="64 64 896 896" focusable="false" data-icon="ellipsis" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                  <path d="M176 511a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0z"></path>
                </svg>
              </span>
            </div>
          </div>
          <div className="listImage_item_tiltel">
            <h4 className={styles.box_loading_title}> </h4>
            <div className={styles.box_loading_content}></div>
          </div>
          <div className={styles.box_loading_image}></div>
          <div className='listImage_item_infor'></div>
        </div>
      </div>
    </div>
    
  )
}

export default LoadBoxContent