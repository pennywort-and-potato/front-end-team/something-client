import React from 'react'
import styles from '../loading/LoadBox.module.scss'
function LoadBoxUser(props: any) {

  return (
    <div className='flex listUser animations-fade-5'>
        <div className='listUser_avatar'></div>
        <div className=''>
            <div className={styles.box_loading_title +" "+ styles.fontSizeUser}></div>
            <div className={styles.box_loading_content +" "+ styles.fontSizeUser}></div>
        </div>
    </div>
    
  )
}

export default LoadBoxUser