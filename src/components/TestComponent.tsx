'use client'

import { userLogin } from '@/api/userApi'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useLocalStorage } from 'usehooks-ts'

function TestComponent() {

  const [ jwt, setJwt ] = useLocalStorage('user_jwt', '')
  const user = useSelector((state: any) => state.user)

  const onSubmit = (e: any) => {
    e.preventDefault()

    const loginForm = new FormData(e.target)
    
    userLogin(loginForm)
    .then((res: any) => setJwt(res.token))
  }

  return (
    <form onSubmit={onSubmit}>
      <label htmlFor='username'>
        Username
      </label>
      <input name='username' />
      <label htmlFor='password'>
        Password
      </label>
      <input name='password' />
      <input name='keepLogin' type='checkbox' value='true' />
      <button type='submit'>Login</button>
    </form>
  )
}

export default TestComponent