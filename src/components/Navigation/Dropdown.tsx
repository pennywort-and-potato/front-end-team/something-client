'use client'
import Link from 'next/link'
import React, { useState } from 'react'
import styles from '../Navigation/Dropdown.module.scss'
export default function Dropdown(props :any) {
    


    const { children, menu,className } = props
    const [showDropdown, setShowDropdown] = useState<boolean>(false) 

    const onClick = (e:any) => {
      
        setShowDropdown(!showDropdown)
    }

    return (
        <div>
            <div onClick={onClick}>
                { children}
            </div>
            {
                showDropdown && (
                <div className={styles.dropdown +" "+ className}>
                    <ul className={styles.dropdownUl}>
                        {menu.map((menuItem:any) => (
                            <li onClick={e => setShowDropdown(!showDropdown)} key={menuItem.key} className={styles.dropdownLi} >{menuItem.label}</li>
				        ))
                        
                        }
                        
                    </ul>

                </div>
                )
            } 
                
          
        </div>
        
    )
}