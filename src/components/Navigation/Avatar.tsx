'use client'
import React from 'react'
import Image from 'next/image';
import { getPost } from '@/api/post' 

import Link from 'next/link';

function Avatar(props: any) {
    

    const {data, isUserLogin} = props
  return (
    <div className="listImage_item_userImg"  >
        <Link href={  isUserLogin ? "/user" : "/user/" + data.user.id}>
            {data.user ? data.user.lastName[0] : ""}
        </Link>
    </div>
  )
}

export default Avatar