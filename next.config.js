/** @type {import('next').NextConfig} */

const path = require('path')

const nextConfig = {
  // output: "export",
  images: { domains:["f005.backblazeb2.com"] },
  sassOptions: {
      includePaths: [path.join(__dirname, 'styles')],
  },
  webpack(config) {
      config.module.rules.push({
        test: /\.svg$/i,
        use: ["@svgr/webpack"],
      });
  
      return config;
    },
    // images: { unoptimized: true },
    trailingSlash: true
}

module.exports = nextConfig
